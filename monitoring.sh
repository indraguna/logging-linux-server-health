#!/bin/bash
#if this script want to run periodically it can run using cron job for interval time
#ex cron entry
#* * * * * /root/checkhealth.sh
#* * * * * sleep 15; /root/checkhealth.sh

#date
date=$(date +"%Y-%m-%d %T")
#disk name
disk=sdb
#network interface
netif=eth0
#command to check all CPU Usage
check_cpu=$(mpstat | grep all | awk '{print $4}')

#command to check memory usage
check_mem=$(free -m | grep Mem | awk '{print $3}')

#command to check read and write disk in Kb/sec
check_rwdisk=$(iostat -x | grep $disk |awk '{print $6","$7}')

#command to check network in and out on eth0 interface in Kb/sec
check_ionet=$(sar -n DEV 1 1 | grep $netif | awk 'NR==1{print $6","$7}')

#name of output file
file=result.txt

#check if output file is exist
#if not create a file and add header
if [ ! -e "$file" ];then
        touch $file
        echo "time,cpu usage,memory usage,read disk Kb/sec,write disk Kb/sec,Network in Kb/sec,Network out Kb/sec" >> $file
        echo $date","$check_cpu","$check_mem","$check_rwdisk","$check_ionet >> $file
else
        echo $date","$check_cpu","$check_mem","$check_rwdisk","$check_ionet >> $file
fi
